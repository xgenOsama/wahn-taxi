module.exports = {
    getAll: async function (userId) {
        let [result, ignored] = await sql.query("SELECT * FROM mobile_rider_tokens WHERE rider_id = ? limit 1", [userId]);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    getToken: async function (userId) {  
        let [result, ignored] = await sql.query("SELECT mobile_token FROM mobile_rider_tokens WHERE rider_id = ? limit 1", [userId]);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    getAndroidToken: async function(userId){
        let [result, ignored] = await sql.query("SELECT mobile_token FROM mobile_rider_tokens WHERE rider_id = ? AND device = ? limit 1", [userId,'android']);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    getIosToken: async function(userId){
        let [result, ignored] = await sql.query("SELECT mobile_token FROM mobile_rider_tokens WHERE rider_id = ? AND device = ? limit 1", [userId,'ios']);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    checkHaveAndroid: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_rider_tokens WHERE rider_id = ? AND device = ? limit 1", [userId,'android']);
        return result;
    },
    checkHaveIos: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_rider_tokens WHERE rider_id = ? AND device = ? limit 1", [userId,'ios']);
        return result;
    },
    updateToken: async function(userId,token){
        let [result, ignored] = await sql.query("UPDATE mobile_rider_tokens SET mobile_token = ? WHERE id = ?", [token,userId]);
        return true;
    },
    insert: async function (token) {
        let [result, ignored] = await sql.query("INSERT INTO mobile_rider_tokens (device, mobile_token, rider_id) VALUES (?, ?, ?)", [token.device, token.mobile_token, token.rider_id]);
        return true;
    },
    update: async function (token) {
        let [result, ignored] = await sql.query("UPDATE mobile_rider_tokens SET device = ?, mobile_token = ?, rider_id = ? WHERE id = ?", [token.device, token.mobile_token, token.rider_id, token.id]);
        return true;
    },
    delete: async function (token) {
        let [result, ignored] = await sql.query("DELETE FROM mobile_rider_tokens WHERE id = ?", [token.id]);
        return true;
    }
};