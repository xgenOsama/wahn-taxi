module.exports = {
    checkHaveNotification: async function (driverId,travelId,type) {
        let [result, ignored] = await sql.query("SELECT * FROM notification_driver_travel WHERE driver_id = ? & travel_id = ? & type = ?  order by id desc limit 1", [driverId,travelId,type]);
        return result[0];
    },
    insert: async function (driverId,travelId,type) {
        let [result, ignored] = await sql.query("INSERT INTO notification_driver_travel (driver_id, travel_id,type) VALUES (?, ?, ?)", [driverId,travelId,type]);
        return true;
    }
};