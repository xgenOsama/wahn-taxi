/** @namespace socket.decoded_token */
const cors = require('cors')();
const express = require('express');
const rp = require('request-promise-any');
const app = express();
app.enable('trust proxy');
require('dotenv').config();
global.dbMigrate = require('db-migrate').getInstance(true);
const bluebird = require('bluebird');
global.fs = bluebird.promisifyAll(require("fs"));
const bodyParser = require('body-parser');
// rp({uri: 'http://31.220.15.49:9000/verify?purchaseCode=' + process.env.PURCHASE_CODE, headers: { 'User-Agent': 'node.js' }, json: true}).then(function(result) {
//     if(result.status === "OK") {
            global.jwtToken = 'HJ7mfNReQ';
            global.riderPrefix = "rider";
            global.driverPrefix = "driver";
            global.current_travels = {};
            global.publicDir = __dirname + "/public/";
            global.mysql = require('./models/mysql');
            global.onesignal = require('./models/onesignal.js');
            global.FCM= require('./models/firebase_node.js');
            global.baseData = [];
            global.serviceTree = [];
            global.drivers = {};
            global.riders = {};
            global.drivers_avalibale_in_background = {};
            global.drivers_avalibale_in_background_n = {};
            global.riders_avalibale_in_background = {};
            app.use(cors);
            app.options('*', cors);
            app.use(bodyParser.json());
            app.use(bodyParser.urlencoded({extended: true}));
            app.use('/img', express.static(__dirname + "/public/img"));
            app.use(express.static('/srv/'));
            app.use(require("./libs/express-router"));
            let server = require('http').createServer(app);
            const io = require("socket.io").listen(server);
            global.operatorsNamespace = require("./libs/operator")(io);
            require("./libs/client")(io);

            process.on('unhandledRejection', r => console.log(r));
            server.listen(process.env.MAIN_PORT, async function () {
                console.log("Listening on " + process.env.MAIN_PORT);
            });




    // } else {
    //     console.log(result.message);
    //     process.exit(1);
    // }
//});
